// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import i18n from './lang/lang'
import store from './store'

Vue.config.productionTip = false
import VueResource from 'vue-resource';
Vue.use(VueResource);
/* eslint-disable no-new */
export const app = new Vue({
  el: '#app',
  store,
  i18n,
  router,
  components: { App },
  template: '<App/>'
})
window['vue'] = app
window.store = store
