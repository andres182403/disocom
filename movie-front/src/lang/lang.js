import Vue from 'vue'
import VueI18n from 'vue-i18n'

import en from './en.json'
import es from './es.json'

Vue.use(VueI18n)
const locale = 'es'
const messages = {
    en: en,
    es: es,
}
const i18n = new VueI18n({
    /** 默认值 */
    locale,
    messages
})
export default i18n