<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;

class MovieController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $movies = Movie::all();
        echo json_encode($movies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $movie = new Movie([
            'title' => $request->title,
            'description' => $request->description,
            'sinopsis' => $request->sinopsis,
            'gender' => $request->gender,
            'img' => 'NO'
        ]);
        $movie->save();
        echo 'Datos almacenados correctamente';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $movie = Movie::find($id);
        $movie->title = $request->title;
        $movie->description = $request->description;
        $movie->sinopsis = $request->sinopsis;
        $movie->gender = $request->gender;
        $movie->img = 'NO';
        $movie->save();
        echo 'Datos actualizados correctamente';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $movie = Movie::find($id);
        $movie->delete();
        echo 'Datos eliminados correctamente';
    }

}
